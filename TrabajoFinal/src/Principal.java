import java.util.Scanner;

public class Principal {

    //Variables Globales
    private static Scanner sc = new Scanner(System.in);
    private static String[] distritos;
    private static String[] productos;
    private static double[] precios;
    private static int maxPedido;
    public static final String RESET = "\033[0m";  // Text Reset
    public static final String RED = "\033[0;31m";     // RED
    public static final String GREEN_BOLD = "\033[1;32m";  // GREEN
    public static final String YELLOW_BOLD = "\033[1;33m"; // YELLOW

    public static void main(String[] args) {
        inicializarDatos();
        String[] datos_comprador = ingresarDatosComprador();
        int[] orden = ingresarOrden();
        cobrarOrden(orden, datos_comprador);
    }

    public static void inicializarDatos() {
        maxPedido = 10;
        productos = new String[]{"Salchipapa Clásica", "Salchipapa Mixta", "Salchupapa Especial", "Broster Alita",
                "Broster Piernota", "Broster Doble Alita", "Broster Pechazo", "Broster Pechuga a la Plancha"};

        precios = new double[]{8.00, 12.0, 14.0, 8.00, 10.0, 12.0, 12.0, 14.0};

        distritos = new String[43];
        distritos[0] = "Ancón";
        distritos[1] = "Ate Vitarte";
        distritos[2] = "Barranco";
        distritos[3] = "Breña";
        distritos[4] = "Carabayllo";
        distritos[5] = "Chaclacayo";
        distritos[6] = "Chorrillos";
        distritos[7] = "Cieneguilla";
        distritos[8] = "Comas";
        distritos[9] = "El Agustino";
        distritos[10] = "Independencia";
        distritos[11] = "Jesús María";
        distritos[12] = "La Molina";
        distritos[13] = "La Victoria";
        distritos[14] = "Lima";
        distritos[15] = "Lince";
        distritos[16] = "Los Olivos";
        distritos[17] = "Lurigancho";
        distritos[18] = "Lurín";
        distritos[19] = "Magdalena del Mar";
        distritos[20] = "Miraflores";
        distritos[21] = "Pachacamac";
        distritos[22] = "Pucusana";
        distritos[23] = "Pueblo Libre";
        distritos[24] = "Puente Piedra";
        distritos[25] = "Punta Hermosa";
        distritos[26] = "Punta Negra";
        distritos[27] = "Rímac";
        distritos[28] = "San Bartolo";
        distritos[29] = "San Borja";
        distritos[30] = "San Isidro";
        distritos[31] = "San Juan de Lurigancho";
        distritos[32] = "San Juan de Miraflores";
        distritos[33] = "San Luis";
        distritos[34] = "San Martín de Porres";
        distritos[35] = "San Miguel";
        distritos[36] = "Santa Anita";
        distritos[37] = "Santa María del Mar";
        distritos[38] = "Santa Rosa";
        distritos[39] = "Santiago de Surco";
        distritos[40] = "Surquillo";
        distritos[41] = "Villa El Salvador";
        distritos[42] = "Villa María del Triunfo";
    }

    public static String[] ingresarDatosComprador() {
        String[] datos_comprador = new String[5];
        /*
        datos_comprador[0] = nombre
        datos_comprador[1] = id distrito
        datos_comprador[2] = direccion
        datos_comprador[3] = dni
        datos_comprador[4] = cel
*/

        System.out.println("Ingrese el nombre completo del cliente: ");
        datos_comprador[0] = sc.nextLine();

        System.out.println("Ingrese identificador de distrito del cliente: ");
        System.out.println("Ingrese [0] para ver el listado de distritos: ");

        String id_distrito;
        do {
            id_distrito = sc.nextLine();
            if (id_distrito.substring(0, 1).equals("0")) {
                mostrarDistritos();
            } else {
                datos_comprador[1] = id_distrito;
            }

        } while (id_distrito.substring(0, 1).equals("0"));

        System.out.println("Ingrese la dirección del cliente: ");
        datos_comprador[2] = sc.nextLine();

        System.out.println("Ingrese el DNI del cliente: ");
        datos_comprador[3] = sc.nextLine();

        System.out.println("Ingrese el número de celular del cliente: ");
        datos_comprador[4] = sc.nextLine();

        return datos_comprador;
    }

    public static int[] ingresarOrden() {
        // Menú
        System.out.println(GREEN_BOLD + "*********Bienvenido a Doña Dona*********");
        System.out.println("*********Barriga llena corazón contento*********" + RESET);
        for (int i = 0; i < productos.length; i++) {
            System.out.println(GREEN_BOLD + "[" + (i + 1) + "] " + RESET + productos[i] + " S./" + precios[i]);
        }
        System.out.println(YELLOW_BOLD + "[0] " + RESET + "Terminar pedido");
        System.out.println("*********Ingrese sus ordenes | Maximo " + maxPedido + " por orden*********");
        //

        int[] orden = new int[maxPedido];
        int id_orden_producto;

        int i = 0;
        do {
            id_orden_producto = sc.nextInt();

            if (id_orden_producto != 0) {
                if (id_orden_producto >= 1 && id_orden_producto <= 8) {
                    orden[i] = id_orden_producto;
                    i++;
                    if (i == maxPedido) {
                        System.out.println(RED + "Maximo de ordenes por persona alcanzado!" + RESET);
                        id_orden_producto = 0;
                    }
                }
            }

        } while (id_orden_producto != 0);

        return orden;
    }

    public static void cobrarOrden(int[] orden, String[] comprador) {

        int cantidad_pedidos = 0;
        double costo_delivery = 0;
        double igv = 0;
        double subtotal = 0;
        double costo_total = 0;


        // Costo Delivery
        for (int i = 0; i < orden.length; i++) {
            if (orden[i] != 0)
                cantidad_pedidos++;
        }
        if (cantidad_pedidos == 0) {
            System.out.println("               Doña Dona SAC               ");
            System.out.println("GRACIAS POR SU PREFERENCIA, VUELVA PRONTO");
            return;
        }
        //Precio delivery por distrito
        switch (Integer.parseInt(comprador[1])) {
            case 15:
                costo_delivery = 2;
                break;
            default:
                costo_delivery = 5;
                break;
        }
        //Más de 5 pedidos, no hay cobro delivery
        if (cantidad_pedidos >= 5) {
            costo_delivery = 0;
        }
        //
        // Costo SubTotal de pedidos
        for (int i = 0; i < orden.length; i++) {
            if (orden[i] != 0) {
                int indece_producto = orden[i] - 1;
                subtotal += precios[indece_producto];
            }
        }

        igv = (costo_delivery + subtotal) * 0.18;
        costo_total = costo_delivery + subtotal + igv;

        System.out.println("               Doña Dona SAC               ");
        System.out.println("CLIENTE");
        System.out.println(comprador[0]);
        System.out.println("DIR. ENVIO");
        int idIdistrito = Integer.parseInt(comprador[1]) - 1;
        System.out.println(comprador[2] + ", " + distritos[idIdistrito]);
        System.out.println("----------------------------------------------");
        for (int i = 0; i < orden.length; i++) {
            if (orden[i] != 0) {
                System.out.printf("%-37s %s %n", productos[orden[i] - 1].toUpperCase(), precios[orden[i] - 1]);
            }
        }
        System.out.println("----------------------------------------------");
        System.out.println("SUBTOTAL                              " + subtotal);
        System.out.println(cantidad_pedidos + " UNIDAD(ES)");
        System.out.println("----------------------------------------------");
        System.out.println("DELIVERY                              " + costo_delivery);
        System.out.println("I.G.V.                                " + igv);
        System.out.println("TOTAL A PAGAR                         " + costo_total);
        System.out.println();
        System.out.println("GRACIAS POR SU PREFERENCIA, VUELVA PRONTO");
        //
    }

    public static void mostrarDistritos() {
        for (int i = 0; i < distritos.length; i++) {
            System.out.println("[" + (i + 1) + "] " + distritos[i]);
        }
    }

}



